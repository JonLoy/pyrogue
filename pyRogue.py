import libtcodpy as libtcod
import math
import textwrap
import shelve

# set game window size
SCREEN_WIDTH = 80
SCREEN_HEIGHT = 50

# Dungeon Map size
MAP_WIDTH = 80
MAP_HEIGHT = 43

# Tile colors
color_dark_wall = libtcod.Color(0,0,100)
color_light_wall = libtcod.Color(130,110,50)
color_dark_ground = libtcod.Color(50,50,150)
color_light_ground = libtcod.Color(200,180,50)

LIMIT_FPS = 20  # maximum of 20 frames per second

# Room constants
ROOM_MAX_SIZE = 10
ROOM_MIN_SIZE = 6
MAX_ROOMS = 30

# Field of View constants
FOV_ALGO = 0 #default FOV algorithm from libtcod
FOV_LIGHT_WALLS = True
TORCH_RADIUS = 10

# Status bar constants
BAR_WIDTH = 20
PANEL_HEIGHT = 7
PANEL_Y = SCREEN_HEIGHT - PANEL_HEIGHT

# Message bar constants
MSG_X = BAR_WIDTH + 2
MSG_WIDTH = SCREEN_WIDTH - BAR_WIDTH -2
MSG_HEIGHT = PANEL_HEIGHT - 1

# Message bar colors
player_death_msg_color = libtcod.red
monster_death_msg_color = libtcod.amber
cancelled_action_msg_color = libtcod.fuchsia
heal_msg_color = libtcod.sky
lightning_msg_color = libtcod.light_yellow
instructions_msg_color = libtcod.han
fire_msg_color = libtcod.flame
confuse_msg_color = libtcod.grey
tranform_msg_color = libtcod.green
ice_msg_color = libtcod.blue
ai_taunt_msg_color = libtcod.dark_red
item_msg_color = libtcod.silver
congrats_msg_color = libtcod.gold
fear_msg_color = libtcod.yellow
slow_msg_color = libtcod.darker_sepia

# game colors
corpse_color = libtcod.dark_red
potion_color = libtcod.purple
scroll_color = libtcod.yellow
common_item_color = libtcod.white
magic_item_color = libtcod.green
rare_item_color = libtcod.blue
epic_item_color = libtcod.purple
legendary_item_color = libtcod.orange

# menu constants
INVENTORY_WIDTH = 50
CHARACTER_SCREEN_WIDTH = 30

# spell constants
ARTIC_BLAST_RADIUS = 3
ARTIC_BLAST_DAMAGE = 4 
ARTIC_BLAST_TURNS = 5
BLIZZARD_RADIUS = 3
BLIZZARD_DAMAGE = 6
BLIZZARD_TURNS = 5
CONFUSE_NUM_TURNS = 10
CONFUSE_RANGE = 8
CORPSE_EXPLOSION_DAMAGE = 5 
CORPSE_EXPLOSION_RANGE = 5
CORPSE_EXPLOSION_RADIUS = 3
DEATH_RANGE = 5
FEAR_NUM_TURNS = 8
FEAR_RANGE = 4
FIREBALL_RADIUS = 3
FIREBALL_DAMAGE = 12 
FROZEN_NUM_TURNS = 4
HEAL_AMOUNT = 4
LIGHTNING_DAMAGE = 20 
LIGHTNING_RANGE = 4
LIFE_DRAIN_DAMAGE = 4
LIFE_DRAIN_RANGE = 4
SHEEP_RANGE = 4
SLOW_NUM_TURNS = 9
SLOW_RANGE = 4
STUN_NUM_TURNS = 5
STUN_DAMAGE = 2
STUN_RANGE = 2
TRADING_PLACES_RANGE = 6

# player leveling constants
LEVEL_UP_BASE = 200
LEVEL_UP_FACTOR = 150
LEVEL_SCREEN_WIDTH = 40

# deities
DEITY_OF_THIEVES = 'Master Locke' 
DEITY_OF_LIFE = 'Lady Nina' 
DEITY_OF_DEATH = 'The Nameless One' 
DEITY_OF_FIRE = 'Lucca of the Flame' 
DEITY_OF_HONOR = 'The Knight of Doma'  
DEITY_OF_KNOWLEDGE = 'Merlin'
DEITY_OF_STORMS = 'Orlandu of the Southern Sky' 
DEITY_OF_NATURE = 'The Mighty Fangorn'  
DEITY_OF_SKY = 'Lord Cid' 
DEITY_OF_OCEANS = 'The Drowned King' 
DEITY_OF_SUN = 'Solaire of Astoria' 
DEITY_OF_MOON = 'Cecil the White' 
DEITY_OF_WARRIORS = 'Erdrick the Brave' 
DEITY_OF_MAGIC = 'The Keeper of the Runes'
DEITY_OF_FATE = 'Warren the Wise' 
DEITY_OF_MERCHANTS = 'Gobi of Prima' 

# enemy constants
GOBLIN_HEALTH = 10
GOBLIN_DEFENSE = 0
GOBLIN_POWER = 3
GOBLIN_XP = 25
HARPY_HEALTH = 12
HARPY_DEFENSE = 0
HARPY_POWER = 4
HARPY_XP = 40
ORC_HEALTH = 8
ORC_DEFENSE = 1
ORC_POWER = 4
ORC_XP = 40
TROLL_HEALTH = 16
TROLL_DEFENSE = 2
TROLL_POWER = 5
TROLL_XP =  75

"""
   Game classes
"""

class Rect:
    """A helper class for drawing rectanglur rooms."""
    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x+w
        self.y2 = y+h

    def center(self):
        """Returns the center of the rectangle."""
        center_x = (self.x1 + self.x2)/2
        center_y = (self.y1 + self.y2)/2
        return (center_x, center_y)

    def intersect(self, other):
        """Determines if this rectangle intersects/overlaps 
        another rectangle. Returns True if it intersects another rectangle.

        """
        return(self.x1 <= other.x2 and self.x2 >= other.x1 and
            self.y1 <= other.y2 and self.y2 >= other.y1)

class Tile:
    """A game_map tile."""
    def __init__(self, blocked, block_sight = None):
        """Initializes a game_map tile.

        Keyword arguments:
        blocked -- determines if tile blocks movement
        block_sight -- determines if tile blocks an Object's block_sight

        """
        self.explored = False
        self.blocked = blocked

        if block_sight is None:
            block_sight = blocked

        self.block_sight = block_sight

class Item:
    """Base class for items."""

    def __init__(self, use_function=None):
        self.use_function = use_function

    def drop(self):
        """Drop item from inventory and place on game_map."""
        objects.append(self.owner)
        inventory.remove(self.owner)
        self.owner.x = player.x
        self.owner.y = player.y
        message('You dropped a ' + self.owner.name, item_msg_color)
        
        if self.owner.equipment:
            self.owner.equipment.unequip()

    def use(self):
        """Calls upon the use_function."""
        # if item is a piece of equipment, equip/unequip
        if self.owner.equipment:
            self.owner.equipment.toggle_equip()
            return
        if self.use_function is None:
            message(self.owner.name + ' can\'t be used.')
        else:
            if self.use_function() != 'cancelled':
                inventory.remove(self.owner)

    def pick_up(self):
        """Add item to player's inventory and remove from the game's map."""
        if len(inventory) >= 26:
            message('Your inventory is full, cannot pick up ' + self.owner.name
             + '.' + cancelled_action_msg_color)
        else:
            inventory.append(self.owner)
            objects.remove(self.owner)
            message('You picked up a ' + self.owner.name + '!', item_msg_color)

            # if slot is empty, equip item
            equipment = self.owner.equipment
            if equipment and get_equipped_in_slot(equipment.slot) is None:
                equipment.equip()

class Equipment:
    """An object to be used by the player."""
    def __init__(self, slot, power_bonus=0, defense_bonus=0, max_hp_bonus=0):
        self.slot = slot
        self.is_equipped = False
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus
        self.max_hp_bonus = max_hp_bonus

    def toggle_equip(self):
        """Toggle equip/unequip status."""
        if self.is_equipped:
            self.unequip()
        else:
            self.equip()

    def equip(self):
        """Equip object and display message.
           Checks if equipment already in slot, if so unequips
           old equipment.
        """ 
        old_equip = get_equipped_in_slot(self.slot)
        if old_equip is not None:
            old_equip.unequip()

        self.is_equipped = True
        message('Equipped ' + self.owner.name + ' on ' + self.slot + '.', 
            item_msg_color )

    def unequip(self):
        """unequip object and display message."""
        if not self.is_equipped: return 
        self.is_equipped = False
        message('Unequipped ' + self.owner.name + ' from ' + self.slot + '.',
            item_msg_color)


class Fighter:
    """Generic warrior."""
    def __init__(self, hp, defense, power, xp, death_function=None):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.base_power = power
        self.xp = xp
        self.death_function = death_function
        self.skills = {}
        self.set_skills()

    @property
    def max_hp(self):
        """Returns current max_hp by adding bonuses to base max_hp."""
        bonus = sum(equipment.max_hp_bonus for eq in get_all_equipped(self.owner))
        return self.base_max_hp + bonus

    @property
    def power(self):
        """Returns current power by adding bonuses to base power."""
        bonus = sum(equipment.power_bonus for eq in get_all_equipped(self.owner))
        return self.base_power + bonus  
      
    @property
    def defense(self):
        """Returns current defense by adding bonuses to base defense."""
        bonus = sum(equipment.defense_bonus for eq in get_all_equipped(self.owner))
        return self.base_defense + bonus
    
    def add_skill(self, skill):
        self.skills[skill] = True

    def set_skills(self):
        self.skills['stun'] = False

    def take_damage(self, damage):
        """Apply damage to player's hp.
           Also handles the death function.

        """
        if damage > 0:
            self.hp -= damage
        if self.hp <= 0:
            function = self.death_function
            if function is not None:
                function(self.owner)

            # give xp to player for killing monster
            if self.owner != player:
                player.fighter.xp += self.xp


    def attack(self, target):
        """Performs attack against target."""
        damage = self.power - target.fighter.defense

        if damage > 0:
            message(self.owner.name.capitalize() + ' attacks ' + target.name + 
                ' for ' + str(damage) + ' damage.')
            target.fighter.take_damage(damage)
        else:
            message(self.owner.name.capitalize() + ' attacks ' + target.name +
             ' but it has no effect.')

    def heal(self, amount):
        """Heals self by specified amount."""
        self.hp += amount
        if self.hp > self.max_hp:
            self.hp = self.max_hp

    def stun(self, target):
        """Does a little damage and stuns the target."""
        old_ai = target.ai
        target.ai = StunnedMonster(old_ai)
        target.ai.owner = target
        message('You stun the ' + target.name + '.', confuse_msg_color)
        target.fighter.take_damage(STUN_DAMAGE*player.level)

"""
   Monster AIs
"""

class BasicMonster:
    """AI for a basic monster. Will move towards player when 
       it is within fov and attack when next to player.

    """
    def take_turn(self):
        """ A basic monster reacts if it is within the fov."""
        monster = self.owner
        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
            # move towards player
            if monster.distance_to(player) >= 2:
                monster.move_towards(player.x, player.y)
            # if within range, attack the player
            elif player.fighter.hp > 0:
                monster.fighter.attack(player)


class ConfusedMonster:
    """AI for a confused monster. Monster will move about randomly and 
       not attack the player.

    """
    def __init__(self, old_ai, num_turns=CONFUSE_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        if self.num_turns > 0:
            self.owner.move(libtcod.random_get_int(0,-1,1), 
                libtcod.random_get_int(0,-1,1))
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message('The ' + self.owner.name + ' recovers its senses.', 
                confuse_msg_color)

class FearedMonster:
    """AI for a scared monster. Monster will move towards the stairs.

    """
    def __init__(self, old_ai, num_turns=FEAR_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns
        (x,y) = find_stairs()
        self.fear_x = x
        self.fear_y = y

    def take_turn(self):     
        if self.num_turns > 0:
            self.owner.move_towards(self.fear_x,self.fear_y)
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message('The ' + self.owner.name + ' overcomes it fear.', 
                fear_msg_color)

class FrozenMonster:
    """AI for a frozen monster. Monster can't move or attack."""
    def __init__(self, old_ai, num_turns=FROZEN_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        if self.num_turns > 0:
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message('The ice block has thawed, and ' + self.owner.name + 
                ' is free.', fire_msg_color)

class SheepedMonster:
    """AI for a sheeped monster. Monster will move about randomly and 
       not attack the player. Slowly regains health while a sheep.

    """
    def __init__(self, old_ai):
        self.old_ai = old_ai

    def take_turn(self):
        self.owner.fighter.heal(1)
        self.owner.move(libtcod.random_get_int(0,-1,1), 
                libtcod.random_get_int(0,-1,1))

class SlowedMonster:
    """AI for a slowed monster. Only able to act every other turn."""
    def __init__(self, old_ai, num_turns=SLOW_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        monster = self.owner
        if self.num_turns > 0:
            if self.num_turns % 2 == 0:
                if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
                    # move towards player
                    distance_to_player = monster.distance_to(player)
                    if distance_to_player > 1:
                        monster.move_towards(player.x, player.y)
                    # if within range, attack the player
                    elif player.fighter.hp > 0 and distance_to_player <= 1:
                        monster.fighter.attack(player)
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message('The ' + monster.name + ' regains its speed.', 
                slow_msg_color)


class StunnedMonster:
    """AI for a stunned monster. Monster will not move nor 
       not attack the player.

    """
    def __init__(self, old_ai, num_turns=STUN_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        if self.num_turns > 0:
            self.num_turns -= 1
        else:
            self.owner.ai = self.old_ai
            message('The ' + self.owner.name + ' recovers its senses.', 
                confuse_msg_color)

class Object:
    """This is a generic object: the player, npc, an item, etc.
       Object is represented on screen by a text character.


    """
    def __init__(self, x, y, char, name, color, blocks=False, always_visible=False, fighter=None, ai=None, item=None, equipment=None):
        """Initialize an Object.

        Keyword arguments:
        x -- horizontal location
        y -- vertical location
        char -- character representing the object
        color -- color of object

        """ 
        self.x = x
        self.y = y
        self.char = char
        self.color = color
        self.name = name
        self.blocks = blocks
        self.always_visible = always_visible
        self.fighter = fighter
        if self.fighter:
            # set owner
            self.fighter.owner = self

        self.ai = ai
        if self.ai:
            # set owner
            self.ai.owner = self

        self.item = item
        if self.item:
            # set owner
            self.item.owner = self

        self.equipment = equipment
        if self.equipment:
            self.equipment.owner = self

            self.item = Item()
            self.item.owner = self

    def move(self, dx, dy):
        """Moves Object in game world.

        Keyword arguments:
        dx -- length of horizontal movement
        dy -- length of vertical movement

        """
        if not is_blocked(self.x + dx, self.y + dy):
            self.x += dx
            self.y += dy

    def move_towards(self, target_x, target_y):
        """ Moves the object towards the target
            using a vector and distance.

        """
        dx = target_x - self.x
        dy = target_y - self.y
        distance = math.sqrt(dx ** 2 + dy ** 2)

        """normalize distance to length 1 (preserving direction)
           then round the distance, and convert it to an integer.
           Thus restricting the distance to the map grid."""
        dx = int(round(dx/distance))
        dy = int(round(dy/distance))
        self.move(dx,dy)

    def distance_to(self, other):
        """Returns the Euclidean distance from self to another object. """
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)

    def distance(self, x, y):
        """Return Euclidean distance to specified coordinates."""
        return math.sqrt((x-self.x) ** 2 + (y-self.y) ** 2)

    def draw(self):
        """Draws Object onto the game screen."""
        if libtcod.map_is_in_fov(fov_map, self.x, self.y) or (self.always_visible
         and game_map[self.x][self.y].explored):
            libtcod.console_set_default_foreground(con, self.color)
            libtcod.console_put_char(con, self.x, self.y, self.char, 
                libtcod.BKGND_NONE)

    def clear(self):
        """Erases Object from screen."""
        libtcod.console_put_char(con, self.x, self.y, 
            ' ', libtcod.BKGND_NONE)

    def send_to_back(self):
        """Draws this object first, so that all others appear above it."""
        global objects
        objects.remove(self)
        objects.insert(0,self)


"""
   Map functions
"""

def make_map():
    """Creates a map with unblocked tiles"""
    global game_map, objects, stairs

    objects = [player]

    # fill game_map with "unblocked" tiles
    game_map = [[Tile(True)
        for y in range(MAP_HEIGHT)]
            for x in range(MAP_WIDTH)]

    rooms = []
    num_rooms = 0

    for r in range(MAX_ROOMS):
        # assign a random height and width to each room
        w = libtcod.random_get_int(0,ROOM_MIN_SIZE,ROOM_MAX_SIZE)
        h = libtcod.random_get_int(0,ROOM_MIN_SIZE,ROOM_MAX_SIZE)
        # randomly place room within map boundaries
        x = libtcod.random_get_int(0,0,MAP_WIDTH-w-1)
        y = libtcod.random_get_int(0,0,MAP_HEIGHT-h-1)

        new_room = Rect(x,y,w,h)

        # check for intersections
        failed = False
        for other_room in rooms:
            if new_room.intersect(other_room):
                failed = True
                break

        if not failed:
            create_room(new_room)

            # add objects to room
            place_monsters(new_room)
            place_items(new_room)

            (new_x, new_y) = new_room.center()

            if num_rooms == 0:
                # this is the first room, the player's starting place.
                player.x = new_x
                player.y = new_y

            else:
                # for all rooms after the first room
                (prev_x, prev_y) = rooms[num_rooms-1].center()

                # connect rooms with tunnels
                if libtcod.random_get_int(0,0,1) == 1:
                    create_h_tunnel(prev_x, new_x, new_y)
                    create_v_tunnel(prev_y, new_y, new_x)
                else:
                    create_v_tunnel(prev_y, new_y, new_x)
                    create_h_tunnel(prev_x, new_x, new_y)
            
            rooms.append(new_room)
            num_rooms += 1   

    # create stairs at the center of the last room created
    stairs = Object(new_x, new_y, '<', 'stairs', libtcod.white)
    objects.append(stairs)
    stairs.send_to_back() # stairs will be drawn below creatures
    stairs.always_visible = True

def create_room(room):
    """Creates a traversable room."""
    global game_map

    for x in range(room.x1+1, room.x2):
        for y in range(room.y1+1, room.y2):
            game_map[x][y].blocked = False
            game_map[x][y].block_sight = False

def create_h_tunnel(x1,x2,y):
    """Creates a horizontal tunnel."""
    global game_map

    for x in range(min(x1,x2),max(x1,x2)-1):
        game_map[x][y].blocked = False
        game_map[x][y].block_sight = False

def create_v_tunnel(y1,y2,x):
    """Creates a vertical tunnel."""
    global game_map

    for y in range(min(y1,y2),max(y1,y2)-1):
        game_map[x][y].blocked = False
        game_map[x][y].block_sight = False

def is_blocked(x, y):
    # first test the game_map tile
    if game_map[x][y].blocked:
        return True

    # now check for any blocking objects
    for obj in objects:
        if obj.blocks and obj.x == x and obj.y == y:
            return True

    return False

def get_names_under_mouse():
    """Returns a string with the name of the objects located under the mouse."""
    global mouse

    (x, y) = (mouse.cx, mouse.cy)

    names = [obj.name for obj in objects 
          if obj.x == x and obj.y == y and libtcod.map_is_in_fov(fov_map, obj.x, obj.y)]
    names = ', '.join(names)

    return names.capitalize()

"""
   General creation functions
"""

def random_choice_index(chances):
    """Returns a random index of a list."""
    dice = libtcod.random_get_int(0, 0, sum(chances))

    running_sum = 0
    index = 0
    for w in chances:
        running_sum += w
        if dice <= running_sum:
           return index
        index += 1

def random_choice(chances_dict):
    """Returns a random key from the dictionary based on probabilty."""
    values = chances_dict.values()
    keys = chances_dict.keys()

    return keys[random_choice_index(values)]

def from_dungeon_level(obj_table):
    """Returns a value based on the level from the table.
       The default is zero.

    """
    for (value, level) in reversed(obj_table):
        if dungeon_level >= level:
            return value
    return 0

"""
   Monster creation and placement functions
"""
def place_monsters(room):
    """Assigns a random number of a variety of monsters to each room."""
    # maximum number of monster per room per level
    max_monsters = from_dungeon_level([[2,1], [3,4], [5,6]])

    # chance for each monster type
    monster_choices = monster_dict()

    # choose random number of monsters
    num_monsters = libtcod.random_get_int(0, 0, max_monsters)

    for i in range(num_monsters):
        # assign monster to random spot in the room
        x = libtcod.random_get_int(0, room.x1+1, room.x2-1)
        y = libtcod.random_get_int(0, room.y1+1, room.y2-1)

        if not is_blocked(x,y):
            choice = random_choice(monster_choices)
            if choice == 'goblin':
                monster = create_goblin(x,y)
            elif choice == 'harpy':
                monster = create_harpy(x,y)
            elif choice == 'orc':
                monster = create_orc(x,y)
            elif choice == 'troll':
                monster = create_troll(x,y)

            objects.append(monster)

def monster_dict():
    """Returns a dictionary of monsters and their probabilty of generation
       per level.

    """
    monster_choices = {}   
    monster_choices['goblin'] = from_dungeon_level([[50,1], [30,3],[15,5],[5,7]])
    monster_choices['harpy'] = from_dungeon_level([[15,1],[30,3],[15,7]])
    monster_choices['orc'] = from_dungeon_level([[10,1],[30,3],[40,5]])
    monster_choices['troll'] = from_dungeon_level([[5,1],[12,3],[25,5],[60,7]])

    return monster_choices

def create_goblin(x,y):
    """Creates a goblin. Goblin can vary in skills and stats based on player's
     level.
     """

    fighter_component = Fighter(hp=GOBLIN_HEALTH*player.level, 
        defense=GOBLIN_DEFENSE*player.level, 
        power=GOBLIN_POWER*player.level, xp=GOBLIN_XP*player.level,
         death_function=monster_death)
    ai_component = BasicMonster()
    monster = Object(x, y, 'g', 'goblin', libtcod.green, 
        blocks=True, fighter=fighter_component, ai=ai_component)

    return monster

def create_harpy(x,y):
    """Creates a harpy. Harpy can vary in skills and stats based on player's
    level.

    """
    fighter_component = Fighter(hp=HARPY_HEALTH*player.level, 
        defense=HARPY_DEFENSE*player.level, 
        power=HARPY_POWER*player.level, xp=HARPY_XP*player.level,
         death_function=monster_death)
    ai_component = BasicMonster()
    monster = Object(x, y, 'h', 'harpy', libtcod.sepia, 
        blocks=True, fighter=fighter_component, ai=ai_component)

    return monster

def create_orc(x,y):
    """Creates an orc. Orc can vary in skills and stats based on player's
    level.

    """
    fighter_component = Fighter(hp=ORC_HEALTH*player.level, 
        defense=ORC_DEFENSE*player.level, 
        power=ORC_POWER*player.level, xp=ORC_XP*player.level,
         death_function=monster_death)
    ai_component = BasicMonster()
    monster = Object(x, y, 'o', 'orc', libtcod.desaturated_green, 
        blocks=True, fighter=fighter_component, ai=ai_component)

    return monster

def create_troll(x,y):
    """Creates a troll. Troll can vary in skills and stats based on player's
    level.

    """
    # create a troll
    fighter_component = Fighter(hp=TROLL_HEALTH*player.level, 
        defense=TROLL_DEFENSE*player.level, 
        power=TROLL_POWER*player.level, xp=TROLL_XP*player.level,
         death_function=monster_death)
    ai_component = BasicMonster()
    monster = Object(x, y, 't', 'troll', libtcod.darker_green, 
        blocks=True, fighter=fighter_component, ai=ai_component)

    return monster

"""
   Item creation and placement functions.
"""
def place_items(room): 
    """Assigns a random number of a variety of items to each room."""
    # maximum number of itmes per room per dungeon level
    max_items = from_dungeon_level([[1,1],[2,4]])

    # chance for each item to appear
    item_choices = item_dict()

    # choose a random number of items
    num_items = libtcod.random_get_int(0, 0, max_items)

    for i in range(num_items):
        # determine a spot to place the item
        x = libtcod.random_get_int(0, room.x1, room.x2)
        y = libtcod.random_get_int(0, room.y1, room.y2)

        # place item if space isn't blocked
        if not is_blocked(x,y):
            choice = random_choice(item_choices)
            item = None
            if choice == 'potion':
                # create a random potion
                potion_choices = potion_dict()
                potion = random_choice(potion_choices)
                item = create_potion(potion, x, y)
            elif choice == 'scroll':
                # create a random scroll
                scroll_choices = scroll_dict()
                scroll = random_choice(scroll_choices)
                item = create_scroll(scroll, x, y)
            elif choice == 'melee':
                # create a melee weapon
                item = create_melee(x,y)
            elif choice == 'shield':
                # create a shield
                item = create_shield(x,y)
            elif choice == 'head':
                # create head armor
                item = create_head(x,y)
            elif choice == 'neck':
                # create neck equipment
                item = create_neck(x,y)
            elif choice == 'shoulders':
                # create shoulders armor
                item = create_shoulders(x,y)
            elif choice == 'chest':
                # create chest armor
                item = create_chest(x,y)
            elif choice == 'back':
                # create back armor
                item = create_back(x,y)
            elif choice == 'wrists':
                # create wrist armor
                item = create_wrists(x,y)
            elif choice == 'hands':
                # create hands armor
                item = create_hands(x,y)
            elif choice == 'waist':
                # create waist armor
                item = create_waist(x,y)
            elif choice == 'ring':
                # create a ring 
                item = create_ring(x,y)
            elif choice == 'legs':
                # create leg armor
                item = create_legs(x,y)
            elif choice == 'feet':
                # create feet armor
                item = create_feet(x,y)
            if item is not None:
                objects.append(item)
                item.send_to_back()
                item.always_visible = True

def item_dict():
    """Returns a dictionary of items with their probabilty of creation 
    per level."""
    item_choices = {}  
    item_choices['potion'] = 30
    item_choices['scroll'] = 15
    item_choices['head'] = 10
    item_choices['neck'] = 10
    item_choices['shoulders'] = 10
    item_choices['chest'] = 10
    item_choices['back'] = 10
    item_choices['wrists'] = 10
    item_choices['hands'] = 10
    item_choices['legs'] = 10
    item_choices['feet'] = 10
    item_choices['ring'] = 8
    item_choices['mainhand'] = 10
    item_choices['offhand'] = 10

    return item_choices

def potion_dict():
    """Returns a potion type to be created."""
    potion_choices = {}
    potion_choices['heal'] = 80
    potion_choices['attack'] = 3
    potion_choices['defense'] = 3

    return potion_choices

def create_potion(potion, x, y):
    item = None
    if potion == 'healing_pot':
        # create a healing potion
        item_component = Item(use_function=cast_heal('potion'))
        item = Object(x,y, '!', 'healing potion', scroll_color, 
            item=item_component)
    elif potion == 'attack':
        # create an attack up potion
        item_component = Item(use_function=use_attack_up)
        item = Object(x,y, '!', 'attack potion', scroll_color, 
            item=item_component)
    elif potion == 'defense':
        # create a defense up potion
        item_component = Item(use_function=use_defense_up)
        item = Object(x,y, '!', 'defense potion', scroll_color, 
            item= item_component)

    return item

def use_attack_up():
    """permanently increases player's attack."""

    message('By the power of ' + str(DEITY_OF_HONOR) + '! You permanently' \
    ' gain + 1 to attack.', congrats_msg_color)

    player.fighter.power += 1

def use_defense_up():
    """permanently increases player's defense."""

    message('By the power of ' + str(DEITY_OF_WARRIORS) + '! You permanently' \
    ' gain + 1 defense.', congrats_msg_color)

    player.fighter.defense += 1

def scroll_dict():
    """Returns a scroll type to be created. """
    scroll_choices = {}
    scroll_choices['artic_blast'] = 10
    scroll_choices['blizzard'] = 10
    scroll_choices['bolt'] = 10
    scroll_choices['confuse'] = 10
    scroll_choices['corpse_exp'] = 10
    scroll_choices['death'] = 10
    scroll_choices['drain'] = 10
    scroll_choices['fear'] = 10
    scroll_choices['fireball'] = 10
    scroll_choices['heal'] = 10
    scroll_choices['reveal'] = 5
    scroll_choices['sheep'] = 10
    scroll_choices['slow'] = 10
    scroll_choices['swap'] = 10
    scroll_choices['teleport'] = 10
    scroll_choices['warp'] = 10

    return scroll_choices

def create_scroll(choice, x, y):
    """Returns a scroll of the type choice as an Item."""
    item = None

    if choice == 'artic_blast':
        # create a scroll of artic blast
        item_component = Item(use_function=cast_artic_blast)
        item = Object(x,y, '?', 'scroll of artic blast',
            scroll_color, item=item_component)
    elif choice == 'blizzard':
        # create a scroll of blizzard
        item_component = Item(use_function=cast_blizzard)
        item = Object(x,y, '?', 'scroll of blizzard',
            scroll_color, item=item_component)
    elif choice == 'bolt':
        # create a lightning bolt scroll
        item_component = Item(use_function=cast_lightning)
        item = Object(x,y, '?', 'scroll of lightning bolt', 
            scroll_color, item= item_component)
    elif choice == 'confuse':             
        # create a scroll of confusion
        item_component = Item(use_function=cast_confuse)
        item = Object(x,y, '?', 'scroll of confusion', 
            scroll_color, item=item_component)
    elif choice == 'corpse_exp':
        # create a scroll of corpse explosion
        item_component = Item(use_function=cast_corpse_explosion)
        item = Object(x,y, '?', 'scroll of corpse explosion',
            scroll_color, item=item_component)
    elif choice == 'death':
        # create a scroll of death
        item_component = Item(use_function=cast_death)
        item = Object(x,y, '?', 'scroll of death',
            scroll_color, item=item_component)
    elif choice == 'drain':
        # create a scroll of life drain
        item_component = Item(use_function=cast_life_drain)
        item = Object(x,y, '?', 'scroll of life drain', 
            scroll_color, item=item_component)
    elif choice == 'fear':
        # create a scroll of fear
        item_component = Item(use_function=cast_fear)
        item = Object(x,y, '?', 'scroll of fear',
            scroll_color, item=item_component)          
    elif choice == 'fireball':                
        # create a scroll of fireball
        item_component = Item(use_function=cast_fireball)
        item = Object(x,y, '?', 'scroll of fireball',
            scroll_color, item=item_component)
    elif choice == 'heal':
        # create a scroll of healing
        item_component = Item(use_function=cast_heal('spell'))
        item = Object(x,y, '?', 'healing potion', potion_color, 
            item=item_component)
    elif choice == 'reveal':
        # create a scroll of reveal items
        item_component = Item(use_function=cast_reveal_items)
        item = Object(x,y, '?', 'scroll of reveal items',
            scroll_color, item=item_component)
    elif choice == 'sheep':
        # create a scroll of sheep
        item_component = Item(use_function=cast_sheep)
        item = Object(x,y, '?', 'scroll of sheep',
            scroll_color, item=item_component)  
    elif choice == 'slow':
        # create a scroll of slow
        item_component = Item(use_function=cast_slow)
        item = Object(x,y, '?', 'scroll of slow',
            scroll_color, item=item_component)   
    elif choice == 'swap':
        # create a scroll of trading places
        item_component = Item(use_function=cast_trading_places)
        item = Object(x,y, '?', 'scroll of trading places',
            scroll_color, item=item_component)
    elif choice == 'teleport':
        # create a scroll of teleporaton
        item_component = Item(use_function=cast_teleport)
        item = Object(x,y, '?', 'scroll of teleporaton',
            scroll_color, item=item_component)
    elif choice == 'warp':
        # create a scroll of warp
        item_component = Item(use_function=cast_warp)
        item = Object(x,y, '?', 'scroll of warp', 
            scroll_color, item=item_component)

    return item

def create_mainhand(x,y):
    """Returns a melee weapon."""
    equipment_component = Equipment(slot='mainhand',power_bonus=3)
    item = Object(x,y, '/', 'sword', common_item_color, 
        equipment=equipment_component)
    
    return item        

def create_offhand(x,y):
    equipment_component = Equipment(slot='offhand',defense_bonus=1)
    item = Object(x,y, '[', 'shield', common_item_color, 
        equipment=equipment_component)
    
    return item

def create_head(x,y):
    equipment_component = Equipment(slot='head', defense_bonus=1)
    item = Object(x,y, '[', 'helmet', common_item_color, 
        equipment=equipment_component)

    return item
           
def create_neck(x,y):
    equipment_component = Equipment(slot='neck', max_hp_bonus=5)
    item = Object(x,y, '\"', 'necklace', common_item_color, 
        equipment=equipment_component)

    return item

def create_shoulders(x,y):
    equipment_component = Equipment(slot='shoulders', defense_bonus=1)
    item = Object(x,y, '[', 'pauldrons', common_item_color, 
        equipment=equipment_component)

    return item

def create_chest(x,y):
    equipment_component = Equipment(slot='chest', defense_bonus=1)
    item = Object(x,y, '[', 'leather jerkin', common_item_color, 
        equipment=equipment_component)
           
def create_back(x,y):
    equipment_component = Equipment(slot='back', power_bonus=1)
    item = Object(x,y, '[', 'cloak', common_item_color, 
        equipment=equipment_component)

    return item

def create_wrists(x,y):
    equipment_component = Equipment(slot='wrists', defense_bonus=1)
    item = Object(x,y, '[', 'bracers', common_item_color, 
        equipment=equipment_component)

    return item

def create_hands(x,y):
    equipment_component = Equipment(slot='hands', defense_bonus=1)
    item = Object(x,y, '[', 'gloves', common_item_color, 
        equipment=equipment_component)

    return item

def create_ring(x,y):
    equipment_component = Equipment(slot='ring', max_hp_bonus=5)
    item = Object(x,y, '=', 'ring', common_item_color, 
        equipment=equipment_component)

    return item

def create_waist(x,y):
    equipment_component = Equipment(slot='waist', power_bonus=1)
    item = Object(x,y, '[', 'belt', common_item_color, 
        equipment=equipment_component)

    return item

def create_legs(x,y):
    equipment_component = Equipment(slot='legs', defense_bonus=1)
    item = Object(x,y, '[', 'greaves', common_item_color, 
        equipment=equipment_component)

    return item

def create_feet(x,y):
    equipment_component = Equipment(slot='feet', defense_bonus=1)
    item = Object(x,y, '[', 'feet', common_item_color, 
        equipment=equipment_component)

    return item
           
def get_equipped_in_slot(slot):
    """Returns the equipment in the slot, or None if slot is empty."""
    for obj in inventory:
        if obj.equipment and obj.equipment.slot == slot and obj.equipment.is_equipped:
            return obj.equipment

    return None

def get_all_equipped(obj):
    if obj == player:
        equipped_list = []
        for item in inventory:
            if item.equipment and item.equipment.is_equipped:
                equipped_list.append(item.equipment)
        return equipped_list
    else:
        return []

"""
   Drawing functions
"""

def render_all():
    """Draws map and objects to game screen."""
    global color_dark_wall
    global color_dark_ground
    global fov_recompute

    if fov_recompute:
        # recompute FOV if needed
        fov_recompute = False
        libtcod.map_compute_fov(fov_map, player.x, player.y, 
            TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO)

        for y in range(MAP_HEIGHT):
            for x in range(MAP_WIDTH):
                visible = libtcod.map_is_in_fov(fov_map, x, y)
                wall = game_map[x][y].block_sight
                if not visible:
                    if game_map[x][y].explored:
                        # game_map is out of player's FOV
                        if wall:
                            libtcod.console_set_char_background(con,x,y,
                                color_dark_wall, libtcod.BKGND_SET)
                        else:
                            libtcod.console_set_char_background(con,x,y,
                                color_dark_ground, libtcod.BKGND_SET)
                else:
                    # is visible to the player
                    if wall:
                        libtcod.console_set_char_background(con,x,y,
                            color_light_wall, libtcod.BKGND_SET)
                    else:
                        libtcod.console_set_char_background(con,x,y,
                            color_light_ground, libtcod.BKGND_SET)
                    game_map[x][y].explored = True

    for obj in objects:
        if obj != player:
            obj.draw()
    player.draw()

    libtcod.console_blit(con,0,0,MAP_WIDTH,MAP_HEIGHT,0,0,0)

    # render the GUI panel
    libtcod.console_set_default_foreground(panel, libtcod.black)
    libtcod.console_clear(panel)

    # render the message box, one line at a time
    y = 1
    for (line, color) in game_msgs:
        libtcod.console_set_default_foreground(panel, color)
        libtcod.console_print_ex(panel, MSG_X, y, libtcod.BKGND_NONE, 
            libtcod.LEFT, line)
        y += 1

    # display player's stats
    if player.fighter:
        render_bar(1, 1, BAR_WIDTH, 'HP', player.fighter.hp, 
            player.fighter.max_hp, libtcod.darker_red, libtcod.light_red)

    libtcod.console_print_ex(panel, 1, 3, libtcod.BKGND_NONE, libtcod.LEFT, 
        'Dungeon level ' + str(dungeon_level))

    # display the names of objects under the mouse
    libtcod.console_set_default_foreground(panel, libtcod.light_gray)
    libtcod.console_print_ex(panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, 
        get_names_under_mouse())

    # blit the contents of the "panel" to the root console
    libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, PANEL_HEIGHT, 0, 0, PANEL_Y)

def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    """Renders a status bar, e.g. HP, Experience, etc."""
    # calculate the width of the status bar
    bar_width = int(float(value) / maximum * total_width)

    # render the background
    libtcod.console_set_default_background(panel, back_color)
    libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)

    # render the bar on top of the background
    libtcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)

    # display status bar text centered
    libtcod.console_set_default_foreground(panel, libtcod.white)
    libtcod.console_print_ex(panel, x+total_width / 2, y, libtcod.BKGND_NONE, 
        libtcod.CENTER, name + ': ' + str(value) + '/' + str(maximum))


"""
   Menu functions
"""

def msgbox(text, width=50):
    menu(text, [], width)

def message(new_msg, color=libtcod.white):
    """Displays message into the message panel."""
    new_msg_lines = textwrap.wrap(new_msg, MSG_WIDTH)

    for line in new_msg_lines:
        # Clear the first line if the messge buffer is full
        if len(game_msgs) == MSG_HEIGHT:
            del game_msgs[0]

        game_msgs.append((line, color))

def menu(header, options, width):
    if len(options) > 26: 
        raise ValueError('Cannot have a menu with more than 26 options.')

    # calculate total height for the header
    header_height = libtcod.console_get_height_rect(con, 0, 0, width, 
        SCREEN_HEIGHT, header)
    if header == '':
        header_height = 0
    height = len(options) + header_height

    # create an off-screen that represents the menu's window
    window = libtcod.console_new(width, height)

    # print auto-wrapped header
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_print_rect_ex(window, 0, 0, width, height, 
        libtcod.BKGND_NONE, libtcod.LEFT, header)

    # print all the options
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ')' + option_text
        libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, 
            libtcod.LEFT, text)
        y += 1
        letter_index += 1

    # blit the contents of "window" to the root console
    x = SCREEN_WIDTH/2 - width/2
    y = SCREEN_HEIGHT/2 - height/2
    libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

    # present root console to player and wait for key-press
    libtcod.console_flush()
    key = libtcod.console_wait_for_keypress(True)

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Left Alt-Enter to toggle FULLSCREEN
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

    # convert ASCII code to index; if it corresponds to an index, return it.
    index = key.c - ord('a')
    if index >= 0 and index < len(options): return index
    return None

def inventory_menu(header):
    """Displays a menu with the items in the inventory."""
    if len(inventory) == 0:
        options = ['Inventory is open']
    else:
        options = []
        for item in inventory:
            text = item.name

            # display additional info if its equipment
            if item.equipment and item.equipment.is_equipped:
                text = text + ' (on ' + item.equipment.slot + ')'
            options.append(text)

    index = menu(header, options, INVENTORY_WIDTH)

    # if an index was chosen, return it
    if index is None or len(inventory) == 0: return None
    return inventory[index].item

def equipment_menu():
    """Displays equipment loadout."""
    equipped = {}
    equipped['head'] = 'empty'
    equipped['neck'] = 'empty'
    equipped['chest'] = 'empty'
    equipped['back'] = 'empty'
    equipped['shoulders'] = 'empty'
    equipped['wrists'] = 'empty'
    equipped['hands'] = 'empty'
    equipped['ring'] = 'empty'
    equipped['waist'] = 'empty'
    equipped['legs'] = 'empty'
    equipped['feet'] = 'empty'

    for item in inventory:
        if item.equipment and item.equipment.is_equipped:
             equipped[item.equipment.slot] = item.name

    msgbox('head        ' + equipped['head'] +
            '\nneck        ' + equipped['neck'] +
            '\nshoulders   ' + equipped['shoulders'] +
            '\nchest       ' + equipped['chest'] +
            '\nback        ' + equipped['back'] +
            '\nwrists      ' + equipped['wrists'] +
            '\nhands       ' + equipped['hands'] +
            '\nring        ' + equipped['ring'] +
            '\nwaist       ' + equipped['waist'] +
            '\nlegs        ' + equipped['legs'] + 
            '\nfeet        ' + equipped['feet'])

def controls_menu():
    """Displays a msgbox with the control commands."""
    """"hjkl - cardinal directions
        yubn - diagonals
    """
    msgbox('\nControls\n\n'\
    'h - left\nj - up\nk - down\nl - right' \
    'y - northwest\nu - northeast\nk - down\nl - right' \
    '@ - character screen\n! - control menu\ni - inventory' \
    '\no - open\nr - read\ng - set skills\nw - pick up\ne - drop' \
    '\n\nAssignable Skills\na - skill 1\ns - skill 2' \
    '\nd - skill 3\nf - skill 4', 30)

"""
   Player and monster functions
"""

def handle_keys():
    """Handles key presses by user."""
    global playerx, playery
    global fov_recompute
    global keys
    
    #real time
    #key = libtcod.console_check_for_keypress()

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Left Alt-Enter to toggle FULLSCREEN
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
    elif key.vk == libtcod.KEY_ESCAPE:
        # exit game
        return 'exit' 

    if game_state == 'playing':
        # movement keys
        if key.vk == libtcod.KEY_UP:
            player_move_or_action(0,-1)
        elif key.vk == libtcod.KEY_DOWN:
            player_move_or_action(0,1)
        elif key.vk == libtcod.KEY_LEFT:
            player_move_or_action(-1,0)
        elif key.vk == libtcod.KEY_RIGHT:
            player_move_or_action(1,0)
        else:
            # test for other keys
            key_char = chr(key.c)
            if key_char == 'h':
                player_move_or_action(-1,0)
            elif key_char == 'j':
                player_move_or_action(0,1)
            elif key_char == 'k':
                player_move_or_action(0,-1)
            elif key_char == 'l':
                player_move_or_action(1,0)
            elif key_char == 'y':
                player_move_or_action(-1,-1)
            elif key_char == 'u':
                player_move_or_action(1,-1)
            elif key_char == 'b':
                player_move_or_action(-1,1)
            elif key_char == 'n':
                player_move_or_action(1,1)
            elif key_char == 'e':
                # pick up item
                for obj in objects:
                    if obj.x == player.x and obj.y == player.y and obj.item:
                        obj.item.pick_up()
                        break
            elif key_char == '!':
                controls_menu()
            elif key_char == '#':
                equipment_menu()
            elif key_char == '@':
                # show character information
                level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
                level_up_xp = level_up_xp - player.fighter.xp
                msgbox('Character Information\n\nLevel: ' + str(player.level) +
                    '\nExperience: ' + str(player.fighter.xp) + '\nFor level up: '
                    + str(level_up_xp) + '\nMaximum HP: ' + str(player.fighter.max_hp) 
                    + '\nAttack: ' + str(player.fighter.power) + '\nDefense:' + 
                    str(player.fighter.defense), CHARACTER_SCREEN_WIDTH)
            elif key_char == 'w':
                # show the inventory, and drop selected item.
                chosen_item = inventory_menu('Press the key next to an item ' \
                    'to drop it from your inventory.\n')
                if chosen_item is not None:
                    chosen_item.drop()
            elif key_char == 'i':
                # display the inventory
                chosen_item = inventory_menu('Press the key next to an item ' \
                    'to use it or any other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.use()
            elif key_char == '<':
                # go down stairs
                if stairs.x == player.x and stairs.y == player.y:
                    next_level()
            elif key_char == 'a':
                if player.fighter.skills['stun']:
                    monster = closest_monster(STUN_RANGE)
                    if monster is None:
                        message('No enemy within range.', cancelled_action_msg_color)
                    player.fighter.stun(monster)
                else:
                    message('Haven\'t learned stun ability.', cancelled_action_msg_color)


            
            return 'didnt-take-turn'

def player_move_or_action(dx, dy):
    """Player attacks if a target exists at the specified spot.
       Otherwise, player moves towards spot."""
    global fov_recompute

    x = player.x + dx
    y = player.y + dy

    # find target
    target = None
    for obj in objects:
        if obj.fighter and obj.x == x and obj.y == y:
            target = obj
            break

    # if target exists attack, otherwise move
    if target is not None:
        player.fighter.attack(target)
    else:
        player.move(dx, dy)
        fov_recompute = True

def check_level_up():
    """Determine if player has recieved enough xp to level up."""
    level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
    if player.fighter.xp >= level_up_xp:
        # player levels up
        player.level += 1
        player.fighter.xp -= level_up_xp
        message('Ding! Your skills have increased!', congrats_msg_color)
        message('You have reached level ' + str(player.level), congrats_msg_color)

        choice = None
        while choice == None:
            choice = menu('Level up! Choose a stat to increase:\n',
                ['Constitution: +20 HP (current max hp: ' + 
                    str(player.fighter.max_hp),
                  'Strength: +1 attack (current attack: ' + 
                    str(player.fighter.power),
                  'Agility: +1 defense (current defense: ' + 
                    str(player.fighter.defense)],
                    LEVEL_SCREEN_WIDTH)

        if choice == 0:
            player.fighter.base_max_hp += 20
        elif choice == 1:
            player.fighter.base_power += 1
        elif choice == 2:
            player.fighter.base_defense += 1

        level_up_heal = player.fighter.max_hp - player.fighter.hp
        player.fighter.heal(level_up_heal)

def player_death(player):
    """The player died and the game is over."""
    global game_state
    
    message('You were killed!', player_death_msg_color)
    game_state = 'dead'

    #transform player into corpse
    player.char = '%'
    player.color = corpse_color
    
    choice = None
    while choice == None:
        choice = menu('YOU DIED!\n', ['Return to game'], 20)

def monster_death(monster):
    """Monster is turned into a corpse.
       The corpse doesn't block, can't attack, and can't move.

    """
    monster.send_to_back()
    message(monster.name.capitalize() + ' is dead! You gain ' 
        + str(monster.fighter.xp) + ' xp.', monster_death_msg_color)
    monster.char = '%'
    monster.color = corpse_color
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name

def closest_monster(max_range):
    """Find the closest monster within the player's FOV."""
    closest_enemy = None
    closest_distance = max_range + 1

    for obj in objects:
        if obj.fighter and not obj == player and libtcod.map_is_in_fov(fov_map, obj.x, obj.y):
            distance = player.distance_to(obj)
            if distance < closest_distance:
                closest_enemy = obj
                closest_distance = distance
    return closest_enemy

def find_stairs():
    """Finds the stairs coordinates and returns them."""
    stairs = None
    for obj in objects:
        if obj.char == '<':
            stairs = obj
    return (stairs.x, stairs.y)

def target_tile(max_range=None):
    """Returns the position of a tile within the player's FOV,
       or None if right-clicked.

    """
    global key, mouse
    while True:
        libtcod.console_flush()
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS|libtcod.EVENT_MOUSE,
            key, mouse)
        render_all()

        (x, y) = (mouse.cx, mouse.cy)

        if mouse.rbutton_pressed or key.vk == libtcod.KEY_ESCAPE:
            return (None, None) # cancel if right click or Escape

        if (mouse.lbutton_pressed and libtcod.map_is_in_fov(fov_map, x, y) and (max_range is None or player.distance(x,y) <= max_range)):
            return (x,y)

def target_monster(max_range=None):
    """ Targets a single monster."""
    while True:
        (x, y) = target_tile(max_range)
        if x is None:
            return None

        # return the first clicked monster, otherwise continue looping
        for obj in objects:
            if obj.x == x and obj.y == y and obj.fighter and obj != player:
                return obj

def target_corpse(max_range=None):
    """Targets a single corpse."""
    while True:
        (x,y) = target_tile(max_range)
        if x is None:
            return None

        # return the first clicked monster, otherwise continue looping
        for obj in objects:
            if obj.x == x and obj.y == y and 'remains' in obj.name:
                return obj

"""
    Spells
"""

def cast_artic_blast():
    """A PBAoE which does ice damage and freezes enemies with its radius."""
    
    message('A torrent of ice explodes from out around you.', ice_msg_color)
    
    for obj in objects:
        if obj != player and obj.fighter: 
            if obj.distance(player.x, player.y) <= ARTIC_BLAST_RADIUS:
                message('The ' + obj.name + ' is dealt ' 
                    + str(ARTIC_BLAST_DAMAGE) + ' frost damage.')
                obj.fighter.take_damage(ARTIC_BLAST_DAMAGE)
                old_ai = obj.ai
                obj.ai = FrozenMonster(old_ai, num_turns=ARTIC_BLAST_TURNS)
                obj.ai.owner = obj
                message('The ' + obj.name + ' is encased in a block of ice.', 
                    ice_msg_color)

def cast_blizzard():
    """Casts an AOE blizzard at a target tile. All monsters within
       the radius are frozen for the duration of the blizzard effect.

    """
    message('Left-click on a tile to cast blizzard.', instructions_msg_color)
    (x, y) = target_tile()
    if x is None: return 'cancelled'

    message('A mountain of snow and ice descends on the area, freezing' \
        ' everything caught by the storm.', ice_msg_color)
    
    damage = BLIZZARD_DAMAGE * player.level

    for obj in objects:
        if obj.distance(x, y) <= BLIZZARD_RADIUS and obj.fighter:
            message('The ' + obj.name + ' is dealt ' + str(damage) + 
                ' frost damage.')
            obj.fighter.take_damage(damage)
            old_ai = obj.ai
            obj.ai = SlowedMonster(old_ai, num_turns=BLIZZARD_TURNS)
            obj.ai.owner = obj
            message('The ' + obj.name + ' slows as ice and snow cling to it.', 
                ice_msg_color)

def cast_confuse():
    """Cast confuse on a single enemy."""
    message('Left-click on an enemy to confuse it.', instructions_msg_color)
    monster = target_monster(CONFUSE_RANGE)
    if monster is None: return 'cancelled'

    # replace the monster's AI with a "confused" AI
    old_ai = monster.ai
    monster.ai = ConfusedMonster(old_ai)
    monster.ai.owner = monster
    message('The eyes of the ' + monster.name + ' go vacant.', 
        confuse_msg_color)
    message('The ' + monster.name + ' begins stumbling around.', 
        confuse_msg_color)

def cast_corpse_explosion():
    """Explodes the remains of a corpse and does damage to all within
    its range.

    """
    message('Left-click on a corpse to explode it.', instructions_msg_color)
    monster = target_corpse(CORPSE_EXPLOSION_RANGE)
    if monster is None: return 'cancelled'

    damage = CORPSE_EXPLOSION_DAMAGE * player.level

    message('You explode ' + str(monster.name) + '.')
    for obj in objects:
        if obj.distance(monster.x, monster.y) <= CORPSE_EXPLOSION_RADIUS and obj.fighter:
            message('The ' + obj.name + ' is burned for ' +
             str(damage) + ' damage.', fire_msg_color)
            obj.fighter.take_damage(damage)
    
    monster.clear()
    objects.remove(monster)


def cast_death():
    """Casts a spell of death, instantly killing an enemy if successful."""
    message('Left-click on an enemy to kill it.', instructions_msg_color)
    monster = target_monster(DEATH_RANGE)
    if monster is None: return 'cancelled'

    message('As you read the scroll you can feel the icy chill of Death' \
        ' fills the room.', libtcod.black)

    chance = libtcod.random_get_int(0,0,100)
    if chance < 60:
        message('Death claims another ' + monster.name + '.', 
            monster_death_msg_color)
        monster.fighter.take_damage(monster.fighter.hp)
    elif chance < 75:
        damage = monster.fighter.hp - player.level 
        message(monster.name + 'barely survives the brush with Death. The ' 
            + monster.name + ' takes ' + str(damage) + ' damage.')
    else:
        message('Death is no one\'s servant.', ai_taunt_msg_color)

def cast_fear():
    """Cast fear on a single enemy. It will attempt to run to the stairs."""
    message('Left-click on an enemy to confuse it.', instructions_msg_color)
    monster = target_monster(FEAR_RANGE)
    if monster is None: return 'cancelled'

    # replace the monster's AI with a "confused" AI
    old_ai = monster.ai
    monster.ai = FearedMonster(old_ai)
    monster.ai.owner = monster
    
    message('Your fearsome stare strikes fear in the heart of the ' + 
        monster.name + '.', fear_msg_color)
    message('The ' + monster.name + ' runs away scared.', confuse_msg_color)

def cast_fireball():
    """Casts an AoE fireball at a target tile."""
    message('Left-click a tile to cast fireball, or right-click to cancel.', 
        instructions_msg_color)
    (x, y) = target_tile()
    if x is None: return 'cancelled'

    message('You hurl a fireball, which explodes, burning everything ' \
        'within its radius.', fire_msg_color)
    
    damage = FIREBALL_DAMAGE * player.level
    
    for obj in objects:
        if obj.distance(x, y) <= FIREBALL_RADIUS and obj.fighter:
            message('The ' + obj.name + ' is burned for ' +
             str(damage) + ' damage.', fire_msg_color)
            obj.fighter.take_damage(damage)

def cast_heal(source):
    """Heals player."""
    if player.fighter.hp == player.fighter.max_hp:
        message('Already at full health.', cancelled_action_msg_color)
        return 'cancelled'
    
    if source == 'spell':
        message('You are bathed in the welcoming light of ' + str(DEITY_OF_LIFE) +
        '. Your wounds are miracously healed.', heal_msg_color)
    else:
        message('You drink a healing potion. Your wounds begin to heal.', 
        heal_msg_color)

    amount = HEAL_AMOUNT * player.level

    message('You gain ' + str(amount) + ' hit points.', heal_msg_color)
    player.fighter.heal(amount)

def cast_life_drain():
    """Takes HP from closest enemy and gives it to the player."""
    monster = closest_monster(LIFE_DRAIN_RANGE)
    if monster is None:
        message('No enemy within range.', cancelled_action_msg_color)
        return 'cancelled'
    
    damage = LIFE_DRAIN_DAMAGE * player.level

    message('Ah, the sweet taste of life.', player_death_msg_color)
    message('You drain ' + str(damage) +' from the ' + 
        monster.name + ' and gain ' + str(damage) + ' health.', 
        player_death_msg_color)
    monster.fighter.take_damage(damage)
    player.fighter.heal(damage)

def cast_lightning():
    """Cast a bolt of lightning at the closest enemy."""
    monster = closest_monster(LIGHTNING_RANGE)
    if monster is None:
        message('No enemy within range.', cancelled_action_msg_color)
        return 'cancelled'
    
    damage = LIGHTNING_DAMAGE * player.level

    message('The air crackles with electricity as you call upon the ' \
        ' power of the storm.', lightning_msg_color)
    message('A lightning bolt strikes ' + monster.name + '.\n', lightning_msg_color)
    message('The ' + monster.name + ' takes ' + str(damage) + 
        ' points of damage.', lightning_msg_color)
    monster.fighter.take_damage(damage)

def cast_reveal_items():
    """Cast a spell to reveal all items on the current dungeon level."""

    message('The revealing light of ' + str(DEITY_OF_THIEVES) + ' illuminates' \
        ' the location of every items on this floor.', item_msg_color)
    for obj in objects:
        if not obj.fighter:
           game_map[obj.x][obj.y].explored = True

def cast_sheep():
    """Transform a monster into a sheep."""
    message('Left-click on an enemy to turn it into a sheep.', 
        instructions_msg_color)
    monster = target_monster(SHEEP_RANGE)
    if monster is None: return 'cancelled'

    message('Presto, change-o', tranform_msg_color)
    message('The ' + monster.name + ' is turned into a sheep.', 
        tranform_msg_color)

    old_ai = monster.ai

    monster.ai = SheepedMonster(old_ai)
    monster.ai.owner = monster
    monster.name = 'sheep'
    monster.char = 's'
    monster.color = libtcod.white

def cast_slow():
    """Slows monster's actions."""
    message('Left-click on an enemy to cast slow on it.', 
        instructions_msg_color)
    monster = target_monster(SLOW_RANGE)
    if monster is None: return 'cancelled'

    message('The ' + monster.name + '\'s movements have slowed.',
        slow_msg_color)

    old_ai = monster.ai
    monster.ai = SlowedMonster(old_ai)
    monster.ai.owner = monster

def cast_teleport():
    """Move to selected tile if its not occupied."""
    message('Left-click a tile to teleport to, or right-click to cancel.', 
        instructions_msg_color)
    (x, y) = target_tile()
    if x is None: return 'cancelled'

    message('You teleport silently to your destination.')
    player.clear()
    player.x = x
    player.y = y

def cast_trading_places():
    """Cast a spell to switch places with a single enemy."""
    message('Left-click on an enemy to trade places with it.', 
        instructions_msg_color)
    monster = target_monster(TRADING_PLACES_RANGE)
    if monster is None: return 'cancelled'

    x = monster.x
    y = monster.y
    monster.x = player.x 
    monster.y = player.y
    player.x = x
    player.y = y

def cast_warp():
    """Warps player to the current level's stairs."""
    (x, y) = find_stairs()
    if x is None: return 'cancelled'
    
    player.x = x
    player.y = y
    map[player.x][player.y].explored = True

"""
    Initialization and Main Game Loop
"""

def save_game():
    """Saves game data to a shelve."""
    file = shelve.open('savegame', 'n')
    file['game_map'] = game_map
    file['objects'] = objects
    file['player_index'] = objects.index(player)
    file['inventory'] = inventory
    file['game_msgs'] = game_msgs
    file['game_state'] = game_state
    file['stairs_index'] = objects.index(stairs)
    file['dungeon_level'] = dungeon_level

    file.close()

def load_game():
    """Load game data from shelve file."""
    global game_map, objects, player, inventory, game_msgs, game_state
    global dungeon_level, stairs

    file = shelve.open('savegame', 'r')
    game_map = file['game_map']
    objects = file['objects']
    player = objects[file['player_index']]
    inventory = file['inventory']
    game_msgs = file['game_msgs']
    game_state = file['game_state']
    dungeon_level = file['dungeon_level']
    stairs = objects[file['stairs_index']]

    file.close()

    initialize_fov()

def new_game():
    """Setup the game, create the FOV map, and start the game loop."""
    global player, inventory, game_msgs, game_state, dungeon_level
    # create Object representing the player
    fighter_component = Fighter(hp=30, defense=2, power=5, xp=0,
        death_function=player_death)
    player = Object(0,0,'@', 'player', libtcod.white, blocks=True, 
        fighter=fighter_component)
    player.level = 1

    dungeon_level = 1

    make_map()
    initialize_fov()

    game_state = 'playing'
    inventory = []

    game_msgs = []

    message('Welcome to your doom, mortal.', ai_taunt_msg_color)

def next_level():
    """Generates a brand new level."""
    global dungeon_level
    message('You stop for a brief respite, before venturing further.', 
        heal_msg_color)
    player.fighter.heal(player.fighter.max_hp/2)
    message('Feeling stronger, you descend further into the dungeon.', 
        instructions_msg_color) 
    dungeon_level += 1
    make_map()
    initialize_fov()

def initialize_fov():
    """Create the field of view map."""
    global fov_map, fov_recompute

    fov_recompute = True  

    fov_map = libtcod.map_new(MAP_WIDTH, MAP_HEIGHT)
    for y in range(MAP_HEIGHT):
        for x in range(MAP_WIDTH):
            libtcod.map_set_properties(fov_map, x, y, not game_map[x][y].block_sight,
                not game_map[x][y].blocked)

    # unexplored areas begin with the default background color
    libtcod.console_clear(con)

def play_game():
    global key, mouse

    player_action = None

    mouse = libtcod.Mouse()
    key = libtcod.Key()

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        # render the game screen
        render_all()
        libtcod.console_flush()

        # remove objects from screen before redrawing them
        for obj in objects:
            obj.clear()

        # handle key presses and exit when Escape is pressed
        player_action = handle_keys()
        if player_action == 'exit':
            save_game()
            break

        # allow the monsters to take their turn
        if game_state == 'playing' and player_action != 'didnt-take-turn':
            for obj in objects:
                if obj.ai:
                    obj.ai.take_turn()
            # check for DoTs and expiring buffs

        # check for level up
        if game_state == 'playing':
            check_level_up()

def main_menu():
    img = libtcod.image_load('menu_background.png')

    while not libtcod.console_is_window_closed():
        # display the background image at twice the resolution of normal
        libtcod.image_blit_2x(img, 0, 0, 0)

        # display game title and creator
        libtcod.console_set_default_foreground(0, libtcod.light_yellow)
        libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2-8, 
            libtcod.BKGND_NONE, libtcod.CENTER, 'DUNGEON OF THE DAMNED')
        libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2-6, 
            libtcod.BKGND_NONE, libtcod.CENTER, 'developed by Jonathan Loy')
        libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2-4, 
            libtcod.BKGND_NONE, libtcod.CENTER, 'http://www.jonloy.com')

        # display menu options
        choice = menu('', ['New Game', 'Load Game', 'Quit'], 24)

        if choice == 0:
            new_game()
            play_game()
        elif choice == 1:
            try:
                load_game()
            except:
                msgbox('\n No saved game to load.\n', 24)
                continue
            play_game()
        elif choice == 2:
            # Quit game
            break


libtcod.console_set_custom_font('arial10x10.png',
libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'pyRogue', False)
libtcod.sys_set_fps(LIMIT_FPS)

# off screen console
con = libtcod.console_new(MAP_WIDTH, MAP_HEIGHT)

# status bar
panel = libtcod.console_new(SCREEN_WIDTH, PANEL_HEIGHT)

main_menu()